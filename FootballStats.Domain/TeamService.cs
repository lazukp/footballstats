﻿using FootballStats.Domain.Model;
using FootballStats.Infrastructure;
using System.Collections.Generic;

namespace FootballStats.Domain
{
    public class TeamService : ITeamService
    {
        public TeamService(ITeamRepository teamRepository)
        {
            TeamRepository = teamRepository;
        }

        public ITeamRepository TeamRepository { get; }

        public IList<Team> GetTeams()
        {
            return TeamRepository.GetAllTeams();
        }
    }
}
