﻿using FootballStats.Domain.Model;
using System.Collections.Generic;

namespace FootballStats.Domain
{
    public interface ITeamService
    {
        IList<Team> GetTeams();
    }
}
