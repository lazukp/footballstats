﻿using FootballStats.Domain;
using Microsoft.AspNetCore.Mvc;

namespace Ekstraklasa.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamService teamService;

        public TeamsController(ITeamService teamService)
        {
            this.teamService = teamService;
        }

        [HttpGet]
        public IActionResult GetTeams()
        {
            var teams = teamService.GetTeams();
            return Ok(teams);
        }
    }
}