using AutoFixture;
using FootballStats.Infrastructure.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace FootballStats.Infrastructure.Test
{
    public sealed class TeamRepositoryTests : IClassFixture<InMemoryDatabaseFixture>, IDisposable
    {
        private readonly Fixture autoFixture;
        private readonly FsContext context;
        private TeamRepository teamRepository;

        public TeamRepositoryTests(InMemoryDatabaseFixture fixture)
        {
            context = fixture.Context;

            autoFixture = new Fixture();
            autoFixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList()
                .ForEach(b => autoFixture.Behaviors.Remove(b));
            autoFixture.Behaviors.Add(new OmitOnRecursionBehavior());
        }

        [Fact]
        public void GetAllTeams_ReturnAllTeams()
        {
            // Arrange
            var teams = GenerateTeams();
            AddTeams(teams);
            teamRepository = new TeamRepository(context);

            // Act
            var result = teamRepository.GetAllTeams();

            // Assert
            Assert.Equal(teams.Count, result.Count);
        }

        private IList<TeamEntity> GenerateTeams()
        {
            IList<TeamEntity> teams = new List<TeamEntity>
            {
                autoFixture.Create<TeamEntity>(),
                autoFixture.Create<TeamEntity>(),
                autoFixture.Create<TeamEntity>()
            };            

            return teams;
        }

        private void AddTeams(IEnumerable<TeamEntity> teams)
        {
            context.Teams.AddRange(teams);
            context.SaveChanges();
        }

        private void CleanUpTeams()
        {
            var allTeams = context.Teams.ToList();
            context.Teams.RemoveRange(allTeams);
            context.SaveChanges();
        }

        public void Dispose()
        {
            CleanUpTeams();
        }
    }
}
