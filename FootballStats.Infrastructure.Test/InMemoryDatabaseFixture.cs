﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Data.Common;

namespace FootballStats.Infrastructure.Test
{
    public sealed class InMemoryDatabaseFixture : IDisposable
    {
        private DbConnection connection;

        public FsContext Context { get; private set; }

        public InMemoryDatabaseFixture()
        {
            if (connection == null)
            {
                connection = new SqliteConnection("DataSource=:memory:");
                connection.Open();
            }

            var options = new DbContextOptionsBuilder<FsContext>()
                .UseSqlite(connection)
                .Options;

            Context = new FsContext(options);
            Context.Database.EnsureCreated();
        }        

        public void Dispose()
        {
            if (Context != null)
            {
                Context.Dispose();
                Context = null;
            }

            if (connection != null)
            {
                connection.Dispose();
                connection = null;
            }
        }
    }
}
