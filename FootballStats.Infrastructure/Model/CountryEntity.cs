﻿using System.Collections.Generic;

namespace FootballStats.Infrastructure.Model
{
    public class CountryEntity : IEntityObject, IAggregateRoot
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<TeamEntity> Teams { get; set; }
    }
}
