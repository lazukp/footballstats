﻿namespace FootballStats.Infrastructure.Model
{
    /// <summary>
    /// Entity objects that implements this interface are aggregate roots that could have its own repository.
    /// </summary>
    public interface IAggregateRoot
    {
    }
}
