﻿namespace FootballStats.Infrastructure.Model
{
    public class TeamEntity : IEntityObject, IAggregateRoot
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public int? YearOfCreation { get; set; }
        public string StreetAddress { get; set; }
        public string Postcode { get; set; }
        public string City { get; set; }
        public int CountryId { get; set; }

        public virtual CountryEntity Country { get; set; }
    }
}
