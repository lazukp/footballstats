﻿namespace FootballStats.Infrastructure
{
    public abstract class BaseRepository
    {
        protected BaseRepository(FsContext context)
        {
            Context = context;
        }

        protected FsContext Context { get; }
    }
}
