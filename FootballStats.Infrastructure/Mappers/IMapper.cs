﻿using FootballStats.Infrastructure.Model;
using FootballStats.Domain.Model;

namespace FootballStats.Infrastructure.Mappers
{
    public interface IMapper<TEntity, TDomain> 
        where TEntity : IEntityObject 
        where TDomain : IDomainObject
    {
        TEntity ToEntity(TDomain domain);
        TDomain ToDomain(TEntity entity);
    }
}
