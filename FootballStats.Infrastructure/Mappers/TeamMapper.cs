﻿using FootballStats.Infrastructure.Model;
using FootballStats.Domain.Model;
using System;

namespace FootballStats.Infrastructure.Mappers
{
    public class TeamMapper : IMapper<TeamEntity, Team>
    {
        private readonly ICountryRepository countryRepository;

        public TeamMapper(ICountryRepository countryRepository)
        {
            this.countryRepository = countryRepository;
        }

        public Team ToDomain(TeamEntity entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            var domain = new Team();
            if (!string.IsNullOrWhiteSpace(entity.StreetAddress) ||
                !string.IsNullOrWhiteSpace(entity.Postcode) ||
                !string.IsNullOrWhiteSpace(entity.City) ||
                entity.Country != null)
            {
                domain.Address = new Address
                {
                    StreetAddress = entity.StreetAddress,
                    Postcode = entity.Postcode,
                    City = entity.City,
                    Country = entity.Country?.Name ?? countryRepository.Get(entity.CountryId)?.Name
                };
            }

            domain.Name = entity.Name;
            domain.ShortName = entity.ShortName;
            domain.YearOfCreation = entity.YearOfCreation;
            return domain;
        }

        public TeamEntity ToEntity(Team domain)
        {
            if (domain == null) throw new ArgumentNullException(nameof(domain));
            var entity = new TeamEntity();
            entity.City = domain.Address?.City;
            entity.CountryId = countryRepository.GetCountryIdByName(domain.Address?.Country);
            entity.Postcode = domain.Address?.Postcode;
            entity.StreetAddress = domain.Address?.StreetAddress;
            entity.Name = domain.Name;
            entity.ShortName = domain.ShortName;
            entity.YearOfCreation = domain.YearOfCreation;
            return entity;
        }
    }
}
