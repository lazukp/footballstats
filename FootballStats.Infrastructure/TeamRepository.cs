﻿using FootballStats.Domain.Model;
using FootballStats.Infrastructure.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FootballStats.Infrastructure
{
    public class TeamRepository : BaseRepository, ITeamRepository
    {
        public TeamRepository(FsContext context) : base(context)
        {
        }

        public void AddTeam(Team teamToAdd)
        {
            throw new NotImplementedException();
        }

        public TeamEntity Get(int id)
        {
            throw new NotImplementedException();
        }

        public IList<Team> GetAllTeams()
        {
            var query = from t in Context.Teams
                        join c in Context.Countries on t.CountryId equals c.Id
                        select new Team
                        {
                            Name = t.Name,
                            ShortName = t.ShortName,
                            YearOfCreation = t.YearOfCreation,
                            Address = new Address
                            {
                                StreetAddress = t.StreetAddress,
                                Postcode = t.Postcode,
                                City = t.City,
                                Country = c.Name
                            }
                        };

            return query.ToList();

        }
    }
}
