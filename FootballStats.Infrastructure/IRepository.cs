﻿using FootballStats.Infrastructure.Model;

namespace FootballStats.Infrastructure
{
    public interface IRepository<T> where T : IAggregateRoot
    {
        T Get(int id);
    }
}
