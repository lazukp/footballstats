﻿using FootballStats.Infrastructure.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FootballStats.Infrastructure.Configurations
{
    public class TeamConfiguration : IEntityTypeConfiguration<TeamEntity>
    {
        public void Configure(EntityTypeBuilder<TeamEntity> builder)
        {
            builder.ToTable("Teams");
            builder.HasKey(e => e.Id);
            builder.HasIndex(e => e.Name).IsUnique();

            builder.Property(e => e.Id)
                .UseSqlServerIdentityColumn();

            builder.Property(e => e.Name)
                .HasMaxLength(256)
                .IsRequired();

            builder.Property(e => e.ShortName)
                .HasMaxLength(64);

            builder.Property(e => e.StreetAddress)
                .HasMaxLength(256);

            builder.Property(e => e.Postcode)
                .HasMaxLength(8)
                .IsUnicode(false);

            builder.Property(e => e.City)
                .HasMaxLength(64)
                .IsRequired();

            AddRelationships(builder);
        }

        private void AddRelationships(EntityTypeBuilder<TeamEntity> builder)
        {
            builder.HasOne(t => t.Country).WithMany(c => c.Teams).HasForeignKey(t => t.CountryId).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
