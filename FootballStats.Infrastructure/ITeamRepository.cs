﻿using FootballStats.Domain.Model;
using FootballStats.Infrastructure.Model;
using System.Collections.Generic;

namespace FootballStats.Infrastructure
{
    public interface ITeamRepository : IRepository<TeamEntity>
    {
        IList<Team> GetAllTeams();
        void AddTeam(Team teamToAdd);
    }
}
