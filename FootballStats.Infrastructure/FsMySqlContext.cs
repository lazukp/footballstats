﻿using FootballStats.Infrastructure.Model;
using FootballStats.Infrastructure.Configurations;
using Microsoft.EntityFrameworkCore;
using FootballStats.Infrastructure.Extensions;

namespace FootballStats.Infrastructure
{
    public class FsMySqlContext : DbContext
    {
        public FsMySqlContext(DbContextOptions<FsMySqlContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CountryConfiguration());
            modelBuilder.ApplyConfiguration(new TeamConfiguration());
            modelBuilder.Seed();

            base.OnModelCreating(modelBuilder);
        }

        public virtual DbSet<CountryEntity> Countries { get; set; }
        public virtual DbSet<TeamEntity> Teams { get; set; }
    }
}
