﻿using FootballStats.Infrastructure.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace FootballStats.Infrastructure
{
    public interface ICountryRepository : IRepository<CountryEntity>
    {
        int GetCountryIdByName(string countryName);
    }
}
