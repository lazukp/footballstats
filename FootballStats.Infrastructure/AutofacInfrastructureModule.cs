﻿using Autofac;

namespace FootballStats.Infrastructure
{
    public class AutofacInfrastructureModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(this.ThisAssembly)
                .Where(t => t.IsSubclassOf(typeof(BaseRepository)))
                .AsImplementedInterfaces();
        }
    }
}
