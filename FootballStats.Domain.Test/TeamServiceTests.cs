using AutoFixture;
using FootballStats.Domain.Model;
using FootballStats.Infrastructure;
using Moq;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace FootballStats.Domain.Test
{
    public class TeamServiceTests
    {
        private TeamService teamService;
        private readonly Fixture autoFixture;

        public TeamServiceTests()
        {
            autoFixture = new Fixture();
            autoFixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList()
                .ForEach(b => autoFixture.Behaviors.Remove(b));
            autoFixture.Behaviors.Add(new OmitOnRecursionBehavior());
        }

        [Fact]
        public void GetTeams_ReturnAllTeams()
        {
            // Arrange
            var teams = GenerateTeams();
            var teamRepositoryMock = new Mock<ITeamRepository>();
            teamRepositoryMock.Setup(x => x.GetAllTeams()).Returns(teams);
            teamService = new TeamService(teamRepositoryMock.Object);

            // Act
            var result = teamService.GetTeams();

            // Assert
            Assert.Equal(teams.Count, result.Count);
            Assert.Equal(teams, result);
        }

        private List<Team> GenerateTeams(int count = 3)
        {
            var teams = new List<Team>();
            for (int i = 0; i < count; i++)
            {
                var team = autoFixture.Create<Team>();
                teams.Add(team);
            }

            return teams;
        }
    }
}
