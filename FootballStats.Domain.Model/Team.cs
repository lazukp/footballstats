﻿namespace FootballStats.Domain.Model
{
    public class Team : IDomainObject
    {
        public string Name { get; set; }
        public string ShortName { get; set; }
        public int? YearOfCreation { get; set; }
        public Address Address { get; set; }
    }
}
