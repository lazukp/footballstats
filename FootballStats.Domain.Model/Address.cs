﻿namespace FootballStats.Domain.Model
{
    public class Address
    {
        public string StreetAddress { get; set; }
        public string Postcode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
    }
}
